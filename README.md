MediaMonitor - 0.0.1alpha
===================

Designed to monitor a local server and network with forecast.io, Plex and more.

Supports MacOSX, Linux, and Windows.

[Live site][ls]

[ls]: https://manson.id.au
